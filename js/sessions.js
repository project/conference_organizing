if (Drupal.jsEnabled) {
  $(document).ready(function() {
  
    // Make the checkboxes on the form toggle signing up or signing off for various conference sessions
     $("input.attendee_link[@type=checkbox]").click( function() { 
       var sid =  $(this).val();
       if ($('#attendee_link_input_'+sid+':checked').val() !== null) { var callback = baseurl+'/session/signup/'+sid; } else { var callback = baseurl+'/session/signoff/'+sid; }
       $('#attendee_link_status_'+sid).parent().parent().toggleClass("attending").toggleClass("notattending");
       $.get(callback, function(data) { $('#attendee_link_status_'+sid).html(data); });
     });
 
    // Make clicking on names of locations populate location box
    $("span.location_option").click( function() { 
      var location = $(this).html();
      $("#edit-field-location-0-value").val(location);
    });

    // Make clicking on names of tracks populate location box
    $("span.track_option").click( function() { 
      var track = $(this).html();
      $("#edit-field-track-0-value").val(track);
    });
    
  });
}
