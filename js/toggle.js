$(document).ready(function() {
    $("#toggle .toggle-more").hide();
    $("#toggle .toggle-link").click(function() {
      $("#toggle .toggle-more").toggle();
      $("#toggle .toggle-less").toggle();
      return false;
    });
});