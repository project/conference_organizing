if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $(".add-links-link a").removeAttr("href");   // Disable javascript fallbacks
    $(".sessions-link-delete-link").removeAttr("href"); // Disable javascript fallbacks

    // Set up the Add Links Functionality
    $(".add-links-link").click( function() { 
      linkwrap = $(".add-links-link .link-wrapper")
      if ($(linkwrap).html() == '<a>Cancel</a>') { 
        $(linkwrap).html('<a>Add link</a');
      } else {
        $(linkwrap).html('<a>Cancel</a>');
      }
      $("#sessions-links-content").slideToggle('medium');
      $("#sessions-link-add").slideToggle('medium');
    });

    // Set up the Delete Links Functionality
    $(".sessions-link-delete-link").click( function() {
     $(this).html('deleting...');
      $.get(baseurl+"/session/delete", { lid: $(this).attr("title"), sid:  $("#sessions-link-sid").val() }, function(data) {
        if (data.legth > 0) { // we have an error
          $("#sessions-link-add-status").html(data).fadeIn('medium');
        }
      });
     // TODO - fix this to be more graceful
     $(this).parent().parent().slideUp('slow').css("display","none");
    });

    // Set up the Submit Links Functionality
    $("#session-link-add-form").submit( function() {
       addform = $("#session-link-add-form");
       $(addform).find("#sessions-link-submit").val('Submitting...');
       $.get(baseurl+"/session/addlink", { sid:  $("#sessions-link-sid").val(), title:  $("#sessions-link-add-title").val(), url:  $("#sessions-link-add-url").val() }, function(data) {
         if (data == null || data.length == 0) { 
           $(addform).find("#sessions-link-add-status").slideUp('slow');
           $.get(baseurl+"/session/linksrefresh", { sid:  $("#sessions-link-sid").val() }, function(data) { 
             $(".add-links-link .link-wrapper").html('<a>Add link</a');
             $(addform).find("#sessions-link-add").slideUp('slow');
             $(addform).find("#sessions-link-add-title").val('');
             $(addform).find("#sessions-link-add-url").val('');
             $("#sessions-links-content").html(data).slideDown('medium');
             $("#sessions-links-content table tr:last a").css("color","red");
             $(addform).find("#sessions-link-submit").val('Submit Link');
           });    
         }
         else { // we have an error
           $(addform).find("#sessions-link-submit").val('Submit Link');
           $(addform).find("#sessions-link-add-status").html(data).fadeIn('medium');
         } 
      });
    return false;
    });  
  });
}
